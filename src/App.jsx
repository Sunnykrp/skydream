import React from 'react';
import {  Routes, Route, BrowserRouter } from 'react-router-dom';
import Home from './Components/Home/Home';
import About from './Components/About/About';
import Leadership from './Components/Leadership/Leadership';
import Vertical from './Components/Vertical/Vertical';
import Contact from './Components/Contact/Contact'
import Navbar from './Components/Navbar/Navbar';
import Hero from './Components/Hero/Hero';
import Gallery from './Components/Gallery/Gallery';
import Footer from './Components/Footer/Footer';
const App = () => {
  return (
    <>
      {/* <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/leadership" element={<Leadership />} />
        <Route path="/vertical" element={<Vertical />} />
        <Route path="/focus" element={<Focus />} />
        <Route path="/contact" element={<Contact/>}/>
      </Routes> */}
      {/* <Contact/> */}
      <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/about" element={<About/>}/>
        <Route path="/vertical" element={<Vertical/>}/>
        <Route path="/leadership" element={<Leadership/>}/>
        <Route path="/gallery" element={<Gallery/>}/>
        <Route path="/contact" element={<Contact/>}/>
      </Routes>
      <Footer/>
      </BrowserRouter>
      {/* <Navbar/>
      <Hero/>
      <About/>
      <Vertical />
      <Leadership/>
      <Gallery/>
      <Contact/>
      <Footer/> */}
       
    </>
  );
};

export default App;
