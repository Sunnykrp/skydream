import React, { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import about_img from '../../assets/Images/about-1.jpg';
import './About.css';
import Banner from '../Banner/Banner';

const About = () => {
  const [showMore, setShowMore] = useState(false);
  const [readMoreClicked, setReadMoreClicked] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();
  const isAboutPage = location.pathname === '/about';

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }, [location]);

  useEffect(() => {
    if (isAboutPage) {
      setShowMore(true);
      setReadMoreClicked(true);
    }
  }, [isAboutPage]);

  const handleReadMoreClick = () => {
    setShowMore(!showMore);
    setReadMoreClicked(true);
    if (!isAboutPage) {
      navigate('/about');
    }
  };

  return (
    <>
      {isAboutPage && <Banner title="ABOUT US" />}
      <div className='container'>
        <h2>About Us</h2>
        <div className='about' id='about'>
          <div className='about-left'>
            <img src={about_img} alt="" className='about-img' />
          </div>

          <div className='about-right'>
            <p>
              Welcome to Skydream Group, a conglomerate dedicated to innovation and excellence across multiple industries. Founded with a vision to redefine standards and pioneer breakthroughs, we are committed to delivering cutting-edge solutions that positively impact lives worldwide. In the realm of computer hardware products, Skydream Group stands at the forefront, specializing in network equipment, firewalls, and other essential components. Our dedication to quality ensures that businesses and individuals alike have access to reliable, high-performance technology that fuels their success and growth. At Skydream, we understand the power of communication and education. That&#39;s why we have ventured into digital language labs, providing immersive and effective tools for language learning and proficiency development.
            </p>
            {showMore ? (
              <>
                <p>
                  Through innovative software and interactive platforms, we empower learners to master new languages with confidence and ease. But our commitment doesn&#39;t end there. Skydream Group is also deeply involved in promoting sustainability and wellness through our organic products line. From eco-friendly alternatives to everyday essentials to nourishing supplements, we strive to foster a healthier planet and population. Furthermore, in the realm of healthcare, we are proud to offer genetic counseling and testing services. With a focus on personalized medicine and proactive healthcare solutions, we empower individuals to make informed decisions about their health and well-being. Adding another dimension to our portfolio, Skydream Group extends its expertise into the realm of tours and travel. With a focus on creating unforgettable experiences, we curate bespoke journeys that immerse travelers in captivating destinations, ensuring memories that last a lifetime. At Skydream Group, our success is driven by our relentless pursuit of excellence, our passion for innovation, and our unwavering commitment to making a positive impact in every community we serve. Join us on our journey as we continue to push boundaries, shape industries, and create a brighter future for generations to come.
                </p>
                {isAboutPage ? null : (
                  <button className='btn btns' onClick={handleReadMoreClick} disabled={readMoreClicked}>Read Less</button>
                )}
              </>
            ) : (
              <button className='btn btns' onClick={handleReadMoreClick} disabled={readMoreClicked}>Read More</button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default About;
