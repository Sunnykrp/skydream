import React from 'react'
import './Banner.css'
const Banner = (props) => {
const { title } = props;
  return (
    <>
        <div className='Banner-hero container'>
        <div className='Banner-hero-text'>
           <h1>{title}</h1>
          
        </div>
    </div>
    </>
  )
}

export default Banner