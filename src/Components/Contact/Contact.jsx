import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { css } from '@emotion/react';
import { RingLoader } from 'react-spinners';
import PlaceIcon from '@mui/icons-material/Place';
import EmailIcon from '@mui/icons-material/Email';
import PhoneIcon from '@mui/icons-material/Phone';
import Banner from '../Banner/Banner';
import './Contact.css';

function Contact() {
  const location = useLocation();
  const isContactPage = location.pathname === '/contact';

  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobile, setMobile] = useState('');
  const [message, setMessage] = useState('');
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);
  const [submitMessage, setSubmitMessage] = useState('');

  // Spinner CSS
  const override = css`
    display: block;
    margin: 0 auto;
    border-color: #38b6ff;
  `;

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }, [location]);

  const validate = () => {
    const newErrors = {};
    if (!firstName) newErrors.firstName = 'First Name is required';
    if (!lastName) newErrors.lastName = 'Last Name is required';
    if (!email) newErrors.email = 'Email is required';
    if (!mobile) newErrors.mobile = 'Mobile number is required';
    if (!message) newErrors.message = 'Message is required';
    return newErrors;
  };

  const sendEmail = async (e) => {
    e.preventDefault();
    const validationErrors = validate();
    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    try {
      setLoading(true);
      const res = await fetch("http://localhost:8006/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          firstName,lastName,email,mobile,message
        })
      });
      if (res.ok) {
        setSubmitMessage('Message sent successfully!');
        setTimeout(() => {
          setSubmitMessage('');
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobile('');
          setMessage('');
          setErrors({});
        }, 3000); // Reset message and form fields after 3 seconds
      } else {
        setSubmitMessage('Message not sent. Please try again.');
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setSubmitMessage('Message not sent. Please try again.');
    }
  }

  return (
    <>
      {isContactPage && <Banner title="CONTACT US" />}
      <div className='container contactUs' id='contact'>
        <div className='title'>
          <h2>Contact Us</h2>
        </div>
        {loading && (
          <div className="spinner">
            <RingLoader color='#38b6ff' css={override} size={50} />
          </div>
        )}
        {submitMessage && (
          <div className={`submit-message ${submitMessage.includes('successfully') ? 'success' : 'error'}`}>
            {submitMessage}
          </div>
        )}
        <div className='box'>
          <div className='contact form'>
            <h3>Get In Touch</h3>
            <form onSubmit={sendEmail}>
              <div className='formBox'>
              <div className='row50'>
  <div className='inputBox'>
    <span>First Name:</span>
    <input
      type='text'
      placeholder='John'
      required
      value={firstName}
      onChange={(e) => setFirstName(e.target.value)} // Update firstName state
    />
    {errors.firstName && <span className='error'>{errors.firstName}</span>}
  </div>
  <div className='inputBox'>
    <span>Last Name:</span>
    <input
      type='text'
      placeholder='Doe'
      required
      value={lastName}
      onChange={(e) => setLastName(e.target.value)} // Update lastName state
    />
    {errors.lastName && <span className='error'>{errors.lastName}</span>}
  </div>
</div>
<div className='row50'>
  <div className='inputBox'>
    <span>Email:</span>
    <input
      type='email'
      placeholder='John@gmail.com'
      required
      value={email}
      onChange={(e) => setEmail(e.target.value)} // Update email state
    />
    {errors.email && <span className='error'>{errors.email}</span>}
  </div>
  <div className='inputBox'>
    <span>Mobile:</span>
    <input
      type='tel'
      placeholder='+91 78********'
      required
      value={mobile}
      onChange={(e) => setMobile(e.target.value)} // Update mobile state
    />
    {errors.mobile && <span className='error'>{errors.mobile}</span>}
  </div>
</div>
<div className='row100'>
  <div className='inputBox'>
    <span>Message:</span>
    <textarea
      placeholder='Write your message here...'
      required
      value={message}
      onChange={(e) => setMessage(e.target.value)} // Update message state
    ></textarea>
    {errors.message && <span className='error'>{errors.message}</span>}
  </div>
</div>

                <div className='row100'>
                  <div className='inputBox'>
                    <input type='submit' value='Send' className='input' required />
                  </div>
                </div>
              </div>
            </form>
          </div>
          {/* Info */}
          <div className='contact info'>
            <h3>Contact Info</h3>
            <div className='infoBox'>
              <div>
                <span><PlaceIcon /></span>
                <p><b>SkyDream Group of Companies</b>
                  <br /> 4th Floor, Mohali Tower, F 539, Phase 8B, Industrial Area, Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055</p>
              </div>
              <div>
                <span><EmailIcon /></span>
                <a href='mailto:hello@skydreamsolutions.in'>hello@skydreamsolutions.in</a>
              </div>
              <div>
                <span><PhoneIcon /></span>
                <a href='tel:+91 9023444183'>+91 9023444183</a>
              </div>
            </div>
          </div>
          {/* Map */}
          <div className='contact map'>
            <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3430.318065925274!2d76.69242902619294!3d30.709457536756094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s4th%20Floor%2C%20Mohali%20Tower%2C%20F%20539%2C%20Phase%208B%2C%20Industrial%20Area%2C%20Sector%2074%2C%20Sahibzada%20Ajit%20Singh%20Nagar%2C%20Punjab%20160055!5e0!3m2!1sen!2sin!4v1711954383408!5m2!1sen!2sin" style={{ border: "0" }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
        </div>
      </div>
    </>
  );
}

export default Contact;
