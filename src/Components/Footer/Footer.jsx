import './Footer.css';
import FacebookIcon from '@mui/icons-material/Facebook';
import flogo from '../../assets/Images/footerlogo.png'
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import MapIcon from '@mui/icons-material/Map';
import EmailIcon from '@mui/icons-material/Email';
import PhoneIcon from '@mui/icons-material/Phone';

const Footer = () => {
    return (
        <div className='container bg-footer'>
            <div className="footer">
                <div className='footer-img'>
                    <img src={flogo} alt="footer logo"/>
                    <div className='footer-logo'>
                        <FacebookIcon className="small-logo" />
                        <LinkedInIcon className="small-logo" />
                        <TwitterIcon className="small-logo" />
                        <InstagramIcon className="small-logo" />
                    </div>
                </div>
                <div className='footer-contact'>
                    <h4>Contact</h4>
                    <div>
                        <div>
                            <p> &#x1F4CD; SkyDream Group of Companies</p>
                            <p>4th Floor, Mohali Tower, F 539, Phase 8B, Industrial Area,<br/> Sector 74, Sahibzada Ajit Singh Nagar, Punjab 160055</p>
                        </div>
                        <div>
                            <p>
                                <EmailIcon className="small-icon" /> helloskydream@gmail.com
                            </p>
                        </div>
                        <div>
                            <p><PhoneIcon className="small-icon" /> +91 9023444183</p>
                        </div>
                    </div>
                </div>
                <div className='footer-content'>
                    <h4>Services</h4>
                    <p>Cloud Consulting</p>
                    <p>Digital Language Labs</p>
                    <p>Buisness Transformation</p>
                    <p>I.T Services</p>
                    <p>Computer Software & Network Equipments</p>
                    <p>Educational Software</p>
                    <p>Tours & Travels</p>
                    <p>Organic Research & Product</p>
                    <p>Genetic Counselling & Research</p>
                </div>
            </div>
            <div className='footer-part'>
                <hr />
                <p>© 2024 TechtweekInfotech | All Rights Reserved | Powered By Techtweek Infotech LLC</p>
            </div>
        </div>
    );
}

export default Footer;
