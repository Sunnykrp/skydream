import React, { useState,useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import './Gallery.css'; 
import Organic1 from '../../assets/Images/Organic1.jpg'; 
import Organic2 from '../../assets/Images/Organic2.jpg'; 
import Organic3 from '../../assets/Images/Organic3.jpg'; 
import It1 from '../../assets/Images/it1.png';
import It2 from '../../assets/Images/it2.png';
import It3 from '../../assets/Images/it3.png';
import It4 from '../../assets/Images/it4.jpeg';
import Edu1 from '../../assets/Images/Edu1.jpg';
import Edu2 from '../../assets/Images/Edu2.jpg';
import Edu3 from '../../assets/Images/Edu3.jpg';
import Edu4 from '../../assets/Images/Edu4.jpeg';
import Edu5 from '../../assets/Images/Edu5.jpeg';
import Manu1 from '../../assets/Images/Manu1.jpg';
import Manu2 from '../../assets/Images/Manu2.jpg';
import Manu3 from '../../assets/Images/Manu3.jpg';
import car1 from '../../assets/Images/car1.jpg';
import car2 from '../../assets/Images/car2.jpg';
import car3 from '../../assets/Images/car3.jpg';
import car4 from '../../assets/Images/car4.jpg';
import car5 from '../../assets/Images/car5.jpg';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer'
import Banner from '../Banner/Banner';

const Gallery = () => {
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [showMore, setShowMore] = useState(false);

    const categories = [
        { id: 1, name: 'Education Technology', images: [Edu1, Edu2, Edu3,Edu4,Edu5] },
        { id: 2, name: 'Organic Farming', images: [Organic1, Organic2, Organic3] },
        { id: 3, name: 'IT Setup/Equipments', images: [It1, It2, It3,It4] },
        { id: 4, name: 'Manufacturing Units', images: [Manu1, Manu2, Manu3] },
        { id: 5, name: 'Transportation', images: [car1, car2, car3,car4,car5] },
        // Add more categories as needed
    ];

    // Set the first category as selected initially
    useState(() => {
        setSelectedCategory(categories[0]);
    });

    const handleCategoryClick = (category) => {
        setSelectedCategory(category);
        setShowMore(false);
    };

    const handleViewMoreClick = () => {
        setShowMore(true);
    };
    const location = useLocation();
   const isGalleryPage=location.pathname==='/gallery'
   useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }, [location]);
    return (
        <>
        {isGalleryPage && <Banner title="GALLERY"/>}
        <div className=" container gallery">
            <h2>Gallery</h2>
            <div>
                {categories.map(category => (
                    <button
                        key={category.id}
                        className={`category-button ${selectedCategory === category ? 'selected-category' : ''}`}
                        onClick={() => handleCategoryClick(category)}
                    >
                        {category.name}
                    </button>
                ))}
            </div>
            {selectedCategory && (
                <div className="images-container">
                    <h3>{selectedCategory.name} Images</h3>
                    <div className="images">
                        {selectedCategory.images.slice(0, showMore ? undefined : 5).map((image, index) => (
                            <img
                                key={`${selectedCategory.name}-${index}`}
                                src={image}
                                alt={`${selectedCategory.name} Image ${index}`}
                                className="image"
                            />
                        ))}
                    </div>
                    
                </div>
            )}
        </div>
        </>
    );
};

export default Gallery;
