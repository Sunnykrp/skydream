import React from 'react'
import './Hero.css'
const Hero=()=> {
  return (
   <>
    <div className='hero container'>
        <div className='hero-text'>
           <h1>Industry Horizons Explored: Unveiling the Diverse Ventures Our Company Ventures Into, Harnessing Innovation and Expertise Across Multiple Sectors for Collective Growth</h1>
           <button className='btn-contact'><a href='mailto:hello@skydreamsolutions.in'>Let's Connect</a></button>
        </div>
    </div>
   </>
  )
}
export default Hero;
