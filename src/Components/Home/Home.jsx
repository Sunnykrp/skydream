import React from 'react'
import Navbar from '../Navbar/Navbar'
import Hero from '../Hero/Hero'
import About from '../About/About'
import Vertical from '../Vertical/Vertical'
import Leadership from '../Leadership/Leadership'
import Gallery from '../Gallery/Gallery'
import Contact from '../Contact/Contact'
import Footer from '../Footer/Footer'

const Home = () => {

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }

  return (
    <>
      <Navbar scrollToTop={scrollToTop} />
      <Hero/>
      <About/>
      <Vertical/>
      <Leadership/>
      <Gallery/>
      <Contact/>
      {/* <Footer/> */}
    </>
  )
}

export default Home;
