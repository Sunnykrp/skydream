import React, { useState,useEffect } from 'react';
import './Leadership.css';
import { useLocation } from 'react-router-dom';
import Banner from '../Banner/Banner';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import CloseIcon from '@mui/icons-material/Close';
import Sahil from '../../assets/Images/SahilSir.jpg';
import Skjha from '../../assets/Images/Skjha.jpg';
import Manjot from '../../assets/Images/Manjot.png';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';

const Leadership = () => {
  const [openSahil, setOpenSahil] = useState(false);
  const [openSkjha, setOpenSkjha] = useState(false);
  const [openManjot, setOpenManjot] = useState(false);

  const handleOpenSahil = () => setOpenSahil(true);
  const handleCloseSahil = () => setOpenSahil(false);

  const handleOpenSkjha = () => setOpenSkjha(true);
  const handleCloseSkjha = () => setOpenSkjha(false);

  const handleOpenManjot = () => setOpenManjot(true);
  const handleCloseManjot = () => setOpenManjot(false);

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '70%',
    height: 'auto',
    bgcolor: 'background.paper',
    border: '2px solid none',
    boxShadow: 24,
    p: 8,
  };
  const location = useLocation();
  const isLeadershipPage = location.pathname === '/leadership';
  
  
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }, [location]);
  return (
    <>
      {isLeadershipPage && <Banner title="OUR LEADERSHIP" />}
      <div className='container Leadership-Container ' id="leader">
        <h2>Our Leadership Team</h2>
        <div className='Leadership-col'>
          <div className='Leadership-row'>
            <div className='Leadership-img' onClick={handleOpenSahil}>
              <img src={Sahil} alt='pic' />
            </div>
            <div className='Leadership-content'>
              <h3>Sahil Dubey</h3>
              <h5>Founder and Chief Managing Director</h5>
              <Modal
                keepMounted
                open={openSahil}
                onClose={handleCloseSahil}
                aria-labelledby='keep-mounted-modal-title'
                aria-describedby='keep-mounted-modal-description'
              >
                <Box sx={style}>
                  <CloseIcon onClick={handleCloseSahil} style={{ position: 'absolute', top: '5px', right: '5px', cursor: 'pointer' }} />
                  <Typography id='keep-mounted-modal-title' variant='h6' component='h2'></Typography>
                  <div className='scrollable-content'>
                    <Typography id='keep-mounted-modal-description' sx={{ ml: 0 }}>
                      {/* Your content here */}
                      <div className='content-para'>
                        <div className='content-img'>
                          <img src={Sahil} style={{ width: '200px', height: '200px' }} alt='pic' />
                          <h3>Sahil Dubey</h3>
                          <h5>Founder and Chief Managing Director</h5>
                          <div className='social-media'>
                            <ul>
                              <li>
                                <a href='https://www.linkedin.com/in/sahil-dubey-075b743a/' target="_blank">
                                  <LinkedInIcon />
                                </a>
                              </li>
                              {/* <li>
                                <a href='#'>
                                  <TwitterIcon />
                                </a>
                              </li>
                              <li>
                                <a href='#'>
                                  <InstagramIcon />
                                </a>
                              </li> */}
                            </ul>
                          </div>
                        </div>
                        <p>
                          Meet Sahil Dubey, the visionary Founder and Chief Managing Director of Skydream Group, whose leadership and expertise have propelled the company to new heights. Sahil is a seasoned professional with a Master's in Computer Application from Sam Higginbottom University of Agriculture, Technology, and Sciences, formerly known as Allahabad Agricultural Institute.

                          With a rich background in Information Technology Services and Business Consulting, Sahil brings over 14 years of invaluable experience to the table. His journey in the corporate world includes serving as a Director at Flo2cash Group Company and leading as the Head of IT Infrastructure and Infosec at Cogneesol.

                          Sahil's passion for innovation and his relentless pursuit of excellence have been the driving forces behind Skydream Group's success. His strategic vision has guided the company in diversifying its portfolio across various sectors, including computer hardware products, digital language labs, organic products, genetic counseling and testing, and tours and travels.

                          Under Sahil's leadership, Skydream Group has emerged as a frontrunner in delivering cutting-edge solutions that cater to the evolving needs of industries and communities worldwide. His commitment to quality, coupled with his entrepreneurial spirit, has positioned the company as a trusted partner for clients seeking transformative and sustainable solutions.

                          Beyond his professional achievements, Sahil is known for his integrity, resilience, and dedication to making a positive impact. He embodies the ethos of leadership, inspiring teams to push boundaries, embrace innovation, and strive for excellence in all endeavors.

                          As Skydream Group continues to chart new territories and shape the future of industries, Sahil Dubey remains steadfast in his commitment to driving growth, fostering innovation, and creating lasting value for stakeholders. With his expertise in business management, IT, and leadership, Sahil continues to lead Skydream Group on its journey towards even greater success and impact.
                        </p>
                      </div>
                    </Typography>
                  </div>
                </Box>
              </Modal>
            </div>
          </div>
          <div className='Leadership-row'>
          <div className='Leadership-img' onClick={handleOpenSkjha}>
            <img src={Skjha} alt='pic' />
          </div>
          <div className='Leadership-content'>
            <h3>Dr. S.K. Jha</h3>
            <h5>Principal Advisor</h5>
            <Modal
              keepMounted
              open={openSkjha}
              onClose={handleCloseSkjha}
              aria-labelledby='keep-mounted-modal-title'
              aria-describedby='keep-mounted-modal-description'
            >
              <Box sx={style}>
                <CloseIcon onClick={handleCloseSkjha} style={{ position: 'absolute', top: '5px', right: '5px', cursor: 'pointer' }} />
                <Typography id='keep-mounted-modal-title' variant='h6' component='h2'></Typography>
                <div className='scrollable-content'>
                <Typography id='keep-mounted-modal-description' sx={{ ml: 0 }}>
                <div className='content-para'>
                    <div className='content-img'>
                      <img src={Skjha} style={{ width: '200px', height: '200px' }} alt='pic' />
                      <h3>Dr. S.K. Jha</h3>
                      <h5>Principle Advisor</h5>
                      <div className='social-media'>
                        <ul>
                          <li>
                            <a href='#'>
                              <LinkedInIcon />
                            </a>
                          </li>
                          {/* <li>
                            <a href='#'>
                              <TwitterIcon />
                            </a>
                          </li>
                          <li>
                            <a href='#'>
                              <InstagramIcon />
                            </a>
                          </li> */}
                        </ul>
                      </div>
                    </div>
                    <p>
                      Dr. S.K. Jha is the founder and managing Partner of Dairy Plant Vision Fresh and Frozen at Village:
                      Shyampura, Tehsil: Haroli, District: Una, H.P. he is also the managing director of Vision academy India
                      PVT LTD. As well as Vision academy of competitions Pvt Ltd, the premier institute for medical and
                      engineering entrance examinations and various other competitive examinations of Northern India having its
                      corporate office at Chandigarh. He is a successful academician, author of Chemistry books for
                      engineering And Medical students and a reputed Chemistry lecturer who made thousands of engineering And
                      medical aspirants dream come true. Moreover, he is the president of children welfare society of India,
                      registered at Shimla in H.P. and children education society of India
                    </p>
                  </div>
                </Typography>
                </div>
              </Box>
            </Modal>
          </div>
        </div>
        <div className='Leadership-row'>
          <div className='Leadership-img' onClick={handleOpenManjot}>
            <img src={Manjot} alt='pic' />
          </div>
          <div className='Leadership-content'>
            <h3>Manjot KS Gill</h3>
            <h5>Principal Advisor</h5>
            <Modal
              keepMounted
              open={openManjot}
              onClose={handleCloseManjot}
              aria-labelledby='keep-mounted-modal-title'
              aria-describedby='keep-mounted-modal-description'
            >
              <Box sx={style}>
                <CloseIcon onClick={handleCloseManjot} style={{ position: 'absolute', top: '5px', right: '5px', cursor: 'pointer' }} />
                <Typography id='keep-mounted-modal-title' variant='h6' component='h2'></Typography>
                <div className='scrollable-content'>
                <Typography id='keep-mounted-modal-description' sx={{ ml: 0 }}>
                <div className='content-para'>
                    <div className='content-img'>
                      <img src={Manjot} style={{ width: '200px', height: '200px' }} alt='pic' />
                      <h3>Manjot KS Gill</h3>
                      <h5>Principle Advisor</h5>
                      <div className='social-media'>
                        <ul>
                          <li>
                            <a href='https://www.linkedin.com/in/manjot-k-s-gill-916ab810/'>
                              <LinkedInIcon />
                            </a>
                          </li>
                          {/* <li>
                            <a href='#'>
                              <TwitterIcon />
                            </a>
                          </li>
                          <li>
                            <a href='#'>
                              <InstagramIcon />
                            </a>
                          </li> */}
                        </ul>
                      </div>
                    </div>
                    <p>
                      Manjot KS Gill is the founder of Mind Ridge Consulting India Pvt Ltd, specializing in Executive
                      Coaching, Business Strategy, and Learning & Development (L&D). With a meaningful and varied experience
                      of 28 years, Manjot dedicated the first 8 years to sharpening his managerial acumen and developing a
                      strong sales and marketing capability. He has coached and taught at prestigious institutions such as
                      IIM Ahmedabad, IIM Bangalore, IIM-S, and IIM-J, showcasing his expertise in creating project teams
                      and nurturing talent. Known for his track record of success in rolling out large projects for clients,
                      Manjot is committed to creating highly successful leaders through executive coaching and strategy
                      execution. His specialties include Executive Coaching, Strategy Execution, Senior Leadership
                      Empowerment, NLP (Neuro-linguistic Programming)-based learning interventions in sales, cross-cultural
                      sensitivity, building leadership pipelines, and strategic decision-making. Manjot has facilitated
                      significant sales launches in the Indian newspaper industry and played a pivotal role in game-changing
                      projects, demonstrating his strategic acumen and leadership capabilities.
                    </p>
                  </div>
                </Typography>
                </div>
              </Box>
            </Modal>
          </div>
        </div>
        </div>
      </div>
    </>
  );
};

export default Leadership;
