import React, { useEffect, useState } from 'react';
import './Navbar.css';
import { NavLink } from 'react-router-dom';
import logoWhite from '../../assets/Images/sky.png';
import logoBlack from '../../assets/Images/skyBlack.png';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';

const Navbar = ({ scrollToTop }) => {
  const [sticky, setSticky] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  
  useEffect(() => {
    const handleScroll = () => {
      window.scrollY > 50 ? setSticky(true) : setSticky(false);
    };

    const handleResize = () => {
      setWindowWidth(window.innerWidth);
      if (window.innerWidth > 768) {
        setShowMenu(false);
      }
    };

    window.addEventListener('scroll', handleScroll);
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const handleMenuClick = () => {
    setShowMenu(!showMenu);
  };

  return (
    <nav className={`container hello ${sticky ? 'dark-nav' : ''}`}>
      <NavLink to="/" className='logo-link' onClick={scrollToTop}>
        <img src={sticky ? logoBlack : logoWhite} alt="" className='logo'/>
      </NavLink>
      {windowWidth <= 768 && !showMenu && <MenuIcon onClick={handleMenuClick} />}
      {windowWidth <= 768 && showMenu && <CloseIcon onClick={handleMenuClick} />}
      <ul className={`nav-links ${windowWidth > 768 || showMenu ? 'show' : ''}`}>
        <li><NavLink activeClassName="active-class" className="navlink" exact to="/" onClick={scrollToTop}>Home</NavLink></li>
        <li><NavLink activeClassName="active-class" className="navlink" exact to="/about">About Us</NavLink></li>
        <li><NavLink activeClassName="active-class" className="navlink" exact to="/vertical">Vertical</NavLink></li>
        <li><NavLink activeClassName="active-class" className="navlink" exact to="/leadership">Our Leadership</NavLink></li>
        <li><NavLink activeClassName="active-class" className="navlink" exact to="/gallery">Gallery</NavLink></li>
        <li><NavLink activeClassName="active-class" className="navlink btn" exact to="/contact">Contact Us</NavLink></li>
      </ul>
    </nav>
  );
};

export default Navbar;
