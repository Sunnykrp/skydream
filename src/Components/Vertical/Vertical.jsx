import React, { useState,useEffect} from "react";
import { useLocation } from 'react-router-dom';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ItS from '../../../src/assets/Images/information-technology.png'
import SyncAltIcon from '@mui/icons-material/SyncAlt';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Navbar from '../Navbar/Navbar';
import './Vertical.css'
import Banner from '../Banner/Banner';
import { faCloud,faDesktop,faBusinessTime,faCab,faMicrochip,faNetworkWired,faLaptopFile,faWheatAwn,faMagnifyingGlass,faScrewdriverWrench } from '@fortawesome/free-solid-svg-icons';
const Vertical = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1550,
        settings: {
          slidesToShow: 4, 
        }
      },
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3, 
        }
      },

      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3, 
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3, 
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2, 
        }
        
      },
      
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 2, 
        }
      },
      {
        breakpoint: 650,
        settings: {
          slidesToShow: 2, 
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1, 
        }
      },
      
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 1, 
        }
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 1, 
        }
      },
     
    ]
  };
  const location = useLocation();
  const isVerticalPage=location.pathname==='/vertical';
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth' // for smooth scrolling
    });
  }, [location]);
  return (
    <>
    {isVerticalPage && <Banner title="VERTICALS" />}
      <div className="container slider-container">
      <h2>Our Verticals</h2>
      <Slider {...settings}>
       
        <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faCloud} />
              </div>
                <h3>Cloud Consulting</h3>
                <p>A sound strategy is the first step in making your Cloud Migration a success. Our Cloud Advisory services help you take a streamlined and holistic approach to your Cloud Initiatives.</p>
            </div>
          </div>
          
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faDesktop} />
              </div>
                <h3>digital Language Labs</h3>
                <p>We have a unique English learning programme that prepares the participant to communicate in English with clarity and confidence. </p>
              
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faBusinessTime} />
              </div>
                <h3>business transformation</h3>
                <p>Moving to the Cloud may seem like a simple decision however there is much to consider. What applications to move and will a given application perform adequately in the Cloud.</p>
              
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faMicrochip} />
              </div>
                <h3>I.T services</h3>
                <p>We deliver real strategic value. Our engineers are experts in optimizing your implementation to save you money and improve your reliability, scalability and security.</p>
              
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faNetworkWired} />
              </div>
                <h3>Computer Software & Network Equipments</h3>
                <p>Network equipment refers to devices facilitating communication and data transmission within a network. Common equipment includes routers, switches, hubs, modems, access points, firewalls, and network interface cards.
                </p>
              
            </div>
          </div>
      
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faLaptopFile} />
              </div>
                <h3>Educational Software</h3>
                <p>Educational software is designed to facilitate learning through interactive digital platforms.
                It encompasses a wide range of programs, including simulations, tutorials, and educational games.
        </p>
              
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faCab} />
              </div>
                <h3>Tours & Travels</h3>
                <p>Tours and Travels offers bespoke journeys worldwide, catering to diverse travel desires. From luxury escapes to adventurous expeditions, our expert team crafts unforgettable experiences.
                </p>
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faWheatAwn} />
              </div>
                <h3>Organic Research & Product</h3>
                <p>The Organic Product Center is a haven for eco-conscious consumers, offering a diverse array of organic goods ranging from fresh produce to sustainable household items.
                </p>
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faMagnifyingGlass} />
              </div>
                <h3>Genetic counselling & Reasearch</h3>
                <p>TGenetic counselling and research involve analyzing individuals' genetic information to provide insights into hereditary conditions, risks, and potential treatments. 
                </p>
            </div>
          </div>
          <div className='column'>
            <div className='card'>
              <div className='icon-wrapper'>
              <FontAwesomeIcon icon={faScrewdriverWrench} />
              </div>
                <h3>Real Estate/Construction</h3>
                <p>TGenetic counselling and research involve analyzing individuals' genetic information to provide insights into hereditary conditions, risks, and potential treatments. 
                </p>
            </div>
          </div>
      </Slider>
    </div>
    </>
  )
}

export default Vertical

